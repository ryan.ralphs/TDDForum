<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ThreadTest extends TestCase
{
    use DatabaseMigrations;

    protected $thread;

    public function setUp()
    {
        parent::setUp();
        $this->thread = factory('App\Thread')->create();
    }
    /**@test */
    public function test_it_has_an_owner()
    {
        $this->assertInstanceOf('App\User', $this->thread->owner);
    }

    public function test_it_has_replies()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->thread->replies);
    }

    public function test_a_thread_can_have_a_reply()
    {
        $this->thread->addReply([
            'body' => 'Foobar',
            'user_id' => '1'
        ]);

        $this->assertCount(1, $this->thread->replies);
    }
}
