@foreach ($thread->replies as $reply)
    <div class="panel-heading">
        <h4> <a href="#">{{$reply->owner->name }} </a> said {{ $reply->created_at->diffForHumans() }}...</h4>
        <div class="panel-body">
            <hr>
            <h7>{{ $reply->body }}</h7>
        </div>
        @endforeach
    </div>