@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <a href="#"> {{$thread->owner->name }} </a> posted:{{ $thread->title }}</div>
                    <div class="panel-body">
                        <hr>
                        <h4>{{ $thread->body }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @include('threads.reply')
                </div>
            </div>
        </div>
    </div>
@endsection
